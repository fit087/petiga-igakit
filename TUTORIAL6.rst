.. role:: option(literal)
.. role:: file(literal)
.. _TUTORIAL6:

Vector-valued problems with Elasticity (*)
==========================================

**Objective:** 

**Assumptions:** 

Differences in the System routine
---------------------------------

Upload derivation somewhere

Boundary Conditions
-------------------

Dirichlet conditions
~~~~~~~~~~~~~~~~~~~~

Constant displacement on a face

Neumann conditions
~~~~~~~~~~~~~~~~~~

Constant traction on a face.

Boundary integrals
~~~~~~~~~~~~~~~~~~

Internal pressure on the cylinder.

Post-processing in igakit
-------------------------

How to compute stresses.
