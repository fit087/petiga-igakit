.. _CONICET:  http://www.conicet.gov.ar/
.. _CIMEC:    http://www.cimec.org.ar/
.. _KAUST:    http://www.kaust.edu.sa/

.. _MPI:      http://www.mpi-forum.org/
.. _PETSc:    http://www.mcs.anl.gov/petsc/
.. _PetIGA: https://bitbucket.org/dalcinl/petiga
.. _igakit:   https://bitbucket.org/dalcinl/igakit

.. _isogeometric analysis:
    http://en.wikipedia.org/wiki/Isogeometric_analysis

.. Local Variables:
.. mode: rst
.. End:
